Title: Big Gym

Group members: 

�	Perillo Salvatore - salvatore.perillo@mail.polimi.it
�	Rebuzzi Damiano - damiano.rebuzzi@mail.polimi.it
�	Rompani Andrea - andrea.rompani@mail.polimi.it

This folder contains the Technology part of the project Big Gym, the developer tool used was Brackets for implementation purpose and Chrome Canary for debugging purpose. We used Jquery to implement the dynamic part of every page of the site and Bootstrap to create the image gallery in some pages. Every page is implemented using pure html.