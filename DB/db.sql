-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: biggym
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Icon` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `FullSizeImage` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `DescriptionHtml` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `BackgroundURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `orderID` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Cardio','cardio.jpg','cardio.jpg','cardioDescription','cardio.jpg',8),(2,'Swimming Pool','swimming.jpg','swimming.jpg','Swimming emerged as a competitive recreational activity in the 1830s in England. In 1828, the first indoor swimming pool, St George\'s Baths was opened to the public.[1] By 1837, the National Swimming Society was holding regular swimming competitions in six artificial swimming pools, built around London. The recreational activity grew in popularity and by 1880, when the first national governing body, the Amateur Swimming Association, was formed, there were already over 300 regional clubs in operation.\n','swimming.jpg',2),(3,'Dance','dance.jpg','dance.jpg','dance description','dance.jpg',7),(6,'Climbing','climbing.jpg','climbing.jpg','climbing description','climbing.jpg',6),(7,'Relaxation','relaxation.jpg','relaxation.jpg','Yoga (/?jo???/; Sanskrit: ???,) is a physical, mental, and spiritual practice or discipline. There is a broad variety of schools, practices and goals in Hinduism, Buddhism (including Vajrayana and Tibetan Buddhism) and Jainism The best-known are Hatha yoga and Raja yoga.\nYoga have benefits against:\nAnxiety and depression.Back pain. \n','relaxation.jpg',1),(8,'Fighting','fighting.jpg','fighting.jpg','Kung fu/Kungfu or Gung fu/Gongfu (Listeni/?k???fu?/ or /?k???fu?/; ??, Pinyin: g?ngfu) is a Chinese term referring to any study, learning, or practice that requires patience, energy, and time to complete, used in the West to refer to Chinese martial arts.Many studies have proved the health benefits of practicing Kung Fu in physical, mental and social aspects, such as increasing anaerobic capacity , muscle strength, balance, flexibility, self-confidence, mood , responsibility, honesty and communication .\n','fighting.jpg',3),(9,'Sport','sport.jpg','sport.jpg','sport desc','sport.jpg',4),(10,'Aerobics','aerobics.jpg','aerobics.jpg','aerobics description','aerobics.jpg',5);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoriesimages`
--

DROP TABLE IF EXISTS `categoriesimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoriesimages` (
  `ImageID` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(11) DEFAULT NULL,
  `Tooltip` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `Type` varchar(45) DEFAULT NULL COMMENT 'IMAGE\nVIDEO',
  `ThumbnailURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `URI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ImageID`),
  KEY `cat_idx` (`CategoryID`),
  CONSTRAINT `cat` FOREIGN KEY (`CategoryID`) REFERENCES `categories` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoriesimages`
--

LOCK TABLES `categoriesimages` WRITE;
/*!40000 ALTER TABLE `categoriesimages` DISABLE KEYS */;
INSERT INTO `categoriesimages` VALUES (1,7,'descrizione','IMAGE','relaxationthumb1.png','relaxation1.png'),(2,7,'descrizione','IMAGE','relaxationthumb2.png','relaxation2.png'),(3,7,'descrizione','IMAGE','relaxationthumb3.png','relaxation3.png'),(4,8,'descrizione','IMAGE','fightingthumb1.png','fighting1.png'),(5,8,'descrizione','IMAGE','fightingthumb2.png','fighting2.png'),(6,8,'descrizione','IMAGE','fightingthumb3.png','fighting3.png'),(7,2,'descrizione','IMAGE','swimmingpoolthumb1.png','swimmingpool1.png'),(8,2,'descrizione','IMAGE','swimmingpoolthumb2.png','swimmingpool2.png'),(9,2,'descrizione','IMAGE','swimmingpoolthumb3.png','swimmingpool3.png');
/*!40000 ALTER TABLE `categoriesimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courselevels`
--

DROP TABLE IF EXISTS `courselevels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courselevels` (
  `LevelID` int(11) NOT NULL AUTO_INCREMENT,
  `LevelName` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `LevelFullName` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `ImageURI` varchar(512) NOT NULL,
  PRIMARY KEY (`LevelID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courselevels`
--

LOCK TABLES `courselevels` WRITE;
/*!40000 ALTER TABLE `courselevels` DISABLE KEYS */;
INSERT INTO `courselevels` VALUES (1,'Basic','Basic','levelBasicIcon.png'),(2,'Medium','Medium','levelMediumIcon.png'),(3,'Advanced','Advanced','levelAdvancedIcon.png'),(4,'Expert','Expert','levelExpertIcon.png'),(5,'Master','Master','levelMasterIcon.png');
/*!40000 ALTER TABLE `courselevels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `CourseID` int(11) NOT NULL AUTO_INCREMENT,
  `Category` int(11) DEFAULT NULL,
  `Level` int(11) DEFAULT NULL,
  `Name` varchar(512) DEFAULT NULL,
  `Description` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `RoomID` int(11) DEFAULT NULL,
  `BackgroundURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`CourseID`),
  KEY `room_idx` (`RoomID`),
  KEY `level_idx` (`Level`),
  KEY `category` (`Category`),
  CONSTRAINT `category` FOREIGN KEY (`Category`) REFERENCES `categories` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `level` FOREIGN KEY (`Level`) REFERENCES `courselevels` (`LevelID`),
  CONSTRAINT `room` FOREIGN KEY (`RoomID`) REFERENCES `rooms` (`RoomID`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,7,1,'Yoga Basic Course ','Yoga (/ˈjoʊɡə/; Sanskrit: योग,) is a physical, mental, and spiritual practice or discipline. There is a broad variety of schools, practices and goals in Hinduism, Buddhism (including Vajrayana and Tibetan Buddhism) and Jainism The best-known are Hatha yoga and Raja yoga.\n\nBasic course is targeted towards who want to learn the basics\n\nNo need to have prior knowledge or skill\n\nJust come and relax like you never done before\n\n\n\n\n\n',1,'yogaBackground.jpg '),(2,7,2,'Yoga Medium Course','Yoga (/ˈjoʊɡə/; Sanskrit: योग,) is a physical, mental, and spiritual practice or discipline. There is a broad variety of schools, practices and goals in Hinduism, Buddhism (including Vajrayana and Tibetan Buddhism) and Jainism The best-known are Hatha yoga and Raja yoga.\n\nBasic course is targeted towards someone who already have praticed basic exercizes in the past but want to know more\n\nRecommended if you have medium-low experience\n\n\n\n\n\n',1,'yogaBackground.jpg'),(3,2,3,'Swimming Advanced Course','Swimming emerged as a competitive recreational activity in the 1830s in England. In 1828, the first indoor swimming pool, St George\'s Baths was opened to the public.[1] By 1837, the National Swimming Society was holding regular swimming competitions in six artificial swimming pools, built around London. The recreational activity grew in popularity and by 1880, when the first national governing body, the Amateur Swimming Association, was formed, there were already over 300 regional clubs in operation across the country\n\nSubscribe to an advanced course if you already have attended the medium course and want to learn more and want to be the best',1,'swimmingBackground.jpg'),(4,7,3,'Yoga Advanced Course','Yoga (/?jo???/; Sanskrit: ???,) is a physical, mental, and spiritual practice or discipline. There is a broad variety of schools, practices and goals in Hinduism, Buddhism (including Vajrayana and Tibetan Buddhism) and Jainism The best-known are Hatha yoga and Raja yoga.\n\nSubscribe to an advanced course if you already have attended the medium course and want to learn more and want to be the best\n\n\n',1,'yogaBackground.jpg'),(5,2,2,'Swimming Medium Course','Swimming emerged as a competitive recreational activity in the 1830s in England. In 1828, the first indoor swimming pool, St George\'s Baths was opened to the public.[1] By 1837, the National Swimming Society was holding regular swimming competitions in six artificial swimming pools, built around London. The recreational activity grew in popularity and by 1880, when the first national governing body, the Amateur Swimming Association, was formed, there were already over 300 regional clubs in operation across the country\n\nBasic course is targeted towards someone who already have praticed basic exercizes in the past but want to know more\n\nRecommended if you have medium-low experience\n\n',2,'swimmingBackground.jpg'),(6,8,1,'Kung Fu Course Basic','Kung fu/Kungfu or Gung fu/Gongfu (Listeni/ˌkʌŋˈfuː/ or /ˌkʊŋˈfuː/; 功夫, Pinyin: gōngfu) is a Chinese term referring to any study, learning, or practice that requires patience, energy, and time to complete, used in the West to refer to Chinese martial arts.[1]\n\nBasic course is targeted towards who want to learn the basics\n\nNo need to have prior knowledge or skill\n\nJust come and relax like you never done before\n',3,'kungfuBackground.jpg'),(7,8,2,'Kung Fu Course Medium','Kung fu/Kungfu or Gung fu/Gongfu (Listeni/ˌkʌŋˈfuː/ or /ˌkʊŋˈfuː/; 功夫, Pinyin: gōngfu) is a Chinese term referring to any study, learning, or practice that requires patience, energy, and time to complete, used in the West to refer to Chinese martial arts.[1]\nBasic course is targeted towards someone who already have praticed basic exercizes in the past but want to know more\n\nRecommended if you have medium-low experience\n\n\n',3,'kungfuBackground.jpg'),(8,8,3,'Kung Fu Course Advanced','Kung fu/Kungfu or Gung fu/Gongfu (Listeni/?k???fu?/ or /?k???fu?/; ??, Pinyin: g?ngfu) is a Chinese term referring to any study, learning, or practice that requires patience, energy, and time to complete, used in the West to refer to Chinese martial arts.[1]\n\nSubscribe to an advanced course if you already have attended the medium course and want to learn more and want to be the best',3,'kungfuBackground.jpg'),(9,2,1,'Swimming Basic Course','Swimming emerged as a competitive recreational activity in the 1830s in England. In 1828, the first indoor swimming pool, St George\'s Baths was opened to the public.[1] By 1837, the National Swimming Society was holding regular swimming competitions in six artificial swimming pools, built around London. The recreational activity grew in popularity and by 1880, when the first national governing body, the Amateur Swimming Association, was formed, there were already over 300 regional clubs in operation across the country\n\nBasic course is targeted towards who want to learn the basics\n\nNo need to have prior knowledge or skill\n\nJust come and relax like you never done before\n\n\n',2,'swimmingBackground.jpg');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courseschedule`
--

DROP TABLE IF EXISTS `courseschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courseschedule` (
  `CourseID` int(11) NOT NULL,
  `ScheduleTable` varchar(50) NOT NULL,
  PRIMARY KEY (`CourseID`),
  CONSTRAINT `course` FOREIGN KEY (`CourseID`) REFERENCES `courses` (`CourseID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courseschedule`
--

LOCK TABLES `courseschedule` WRITE;
/*!40000 ALTER TABLE `courseschedule` DISABLE KEYS */;
INSERT INTO `courseschedule` VALUES (1,'schedule.jpg'),(2,'schedule.jpg'),(3,'schedule.jpg'),(4,'schedule.jpg'),(5,'schedule.jpg'),(6,'schedule.jpg'),(7,'schedule.jpg'),(8,'schedule.jpg'),(9,'schedule.jpg');
/*!40000 ALTER TABLE `courseschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipment` (
  `EquipmentID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `Description` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `ImageURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`EquipmentID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipment`
--

LOCK TABLES `equipment` WRITE;
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gymcontacts`
--

DROP TABLE IF EXISTS `gymcontacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gymcontacts` (
  `Seat` varchar(50) NOT NULL,
  `TelephoneNumber` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `FacebookContact` varchar(50) NOT NULL,
  PRIMARY KEY (`Seat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gymcontacts`
--

LOCK TABLES `gymcontacts` WRITE;
/*!40000 ALTER TABLE `gymcontacts` DISABLE KEYS */;
INSERT INTO `gymcontacts` VALUES ('main','+39123456789','reception@biggym.com','facebook.com/BIGGYM');
/*!40000 ALTER TABLE `gymcontacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gymlocations`
--

DROP TABLE IF EXISTS `gymlocations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gymlocations` (
  `Seat` varchar(50) NOT NULL,
  `SNumber` varchar(50) NOT NULL,
  `Street` varchar(50) NOT NULL,
  `City` varchar(50) NOT NULL,
  `Province` varchar(50) NOT NULL,
  `PostCode` int(50) NOT NULL,
  PRIMARY KEY (`Seat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gymlocations`
--

LOCK TABLES `gymlocations` WRITE;
/*!40000 ALTER TABLE `gymlocations` DISABLE KEYS */;
INSERT INTO `gymlocations` VALUES ('main','5021','Garzotto Ave','Milan','(MI)',20121);
/*!40000 ALTER TABLE `gymlocations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructorimages`
--

DROP TABLE IF EXISTS `instructorimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructorimages` (
  `ContentID` int(11) NOT NULL AUTO_INCREMENT,
  `EmployeeCode` int(11) DEFAULT NULL,
  `Type` varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT 'VIDEO\nIMAGE\nSOUND',
  `PageLocation` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT 'AWARDS\nRIGHT',
  `URI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ContentID`),
  KEY `Instructor` (`EmployeeCode`),
  CONSTRAINT `Instructor` FOREIGN KEY (`EmployeeCode`) REFERENCES `instructors` (`EmployeeCode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructorimages`
--

LOCK TABLES `instructorimages` WRITE;
/*!40000 ALTER TABLE `instructorimages` DISABLE KEYS */;
INSERT INTO `instructorimages` VALUES (1,1000,'IMAGE','AWARDS','prize1.jpg'),(2,1000,'IMAGE','RIGHT','arnoldgallery1.jpg'),(3,1000,'IMAGE','RIGHT','arnoldgallery2.jpg'),(4,1000,'IMAGE','RIGHT','arnoldgallery3.jpg'),(5,1004,'IMAGE','AWARDS','prize1.jpg'),(6,1004,'IMAGE','RIGHT','chuckgallery1.jpg'),(7,1004,'IMAGE','RIGHT','chuckgallery2.jpg'),(8,1004,'IMAGE','RIGHT','chuckgallery3.jpg'),(9,1007,'IMAGE','AWARDS','prize1.jpg'),(10,1007,'IMAGE','RIGHT','terrygallery1.jpg'),(11,1007,'IMAGE','RIGHT','terrygallery2.jpg'),(12,1007,'IMAGE','RIGHT','terrygallery3.jpg');
/*!40000 ALTER TABLE `instructorimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructors`
--

DROP TABLE IF EXISTS `instructors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructors` (
  `EmployeeCode` int(11) NOT NULL,
  `FirstName` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `LastName` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `SubTitle` varchar(512) CHARACTER SET utf8 NOT NULL,
  `Email` varchar(43) NOT NULL,
  `Description` varchar(2000) CHARACTER SET utf8 DEFAULT NULL,
  `PortraitURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `SmallSquareURI` varchar(512) NOT NULL,
  `TwitterID` varchar(512) NOT NULL,
  `FacebookID` varchar(512) NOT NULL,
  PRIMARY KEY (`EmployeeCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructors`
--

LOCK TABLES `instructors` WRITE;
/*!40000 ALTER TABLE `instructors` DISABLE KEYS */;
INSERT INTO `instructors` VALUES (1000,'Arnold','Schwarzenegger','Yoga and Aerobics trainer','shwarzy@biggym.com','Is an Austrian-born American actor, model, producer, director, activist, businessman, investor, writer, philanthropist, former professional bodybuilder, and politician. Schwarzenegger served two terms as the 38th Governor of California from 2003 until 2011. Schwarzenegger began weight training at the age of 15. He won the Mr. Universe title at age 20 and went on to win the Mr. Olympia contest seven times. Schwarzenegger has remained a prominent presence in bodybuilding and has written many books and articles on the sport. Schwarzenegger gained worldwide fame as a Hollywood action film icon.','arnold.jpg','arnold.png','',''),(1001,'Andrew','Sportingale','Profession','a.sportingale@biggym.com','Andrew Sportingale is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','sportingale.jpg','sportingale.png','',''),(1003,'Beppe','Brugola','profession','b.brugola@biggym.com','beppe brugola description','brugola.jpg','brugola.png','',''),(1004,'Chuck','Norris','Karate and Kung Fu master','roundhouse@biggym.com','(born July 30, 1940) is an American martial artist, actor and aerobics guru.\nHe became hugely famous thanks of his godlike ability to kick, he so rarely use it that none of us at big gym has ever had the immense pleasure to see it.\nChuck is a gentle giant, always ready to help the others, some say he seems a \"texas ranger\".\nChuck made history in 1990 when he was the first Westerner in the documented history of Tae Kwon Do to be given the rank of 8th Degree Black Belt Grand Master.\nHe teaches Aerobics and karate to everyone spreading its knowledge and lifestile, while cultivating your talents and removing any of your weeknesses','norris.jpg','norris.png','',''),(1005,'Casio','Sharp','profession','c.sharp@biggym.com','Casio Sharp  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','sharp.jpg','sharp.png','',''),(1006,'Frank','A Tee','profession','f.atee@biggym.com','Frank a tee  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','atee.jpg','atee.png','',''),(1007,'Terry','Crews','Professional Swimmer','ceasar@biggym.com','(born July 30, 1968) is an American actor and swimmer.\nHe is best known for his 2 olympic medals and 3 academy awards won while filming \"I, Me and the waves\"\nHe also is a very good singer who likes to sing \"ebony and ivory\" with stevie wonder\nFrom 2007 he teaches our swimming course using all his knowledge he gained while competing on professional level, if you want to be the best of the best, he can mold to be a winner!\n','crews.jpg','crews.png','',''),(1008,'Ge','Force','profession','geforce@biggym.com','Ge Force  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','geforce.jpg','geforce.png','',''),(1009,'Tamara','N vidia','profession','t.nvidia@biggym.com','Tamara N vidia  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','nvidia.jpg','nvidia.png','',''),(1010,'Lee','Novo','profession','l.novo@biggym.com','Lee Novo  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','novo.jpg','novo.png','',''),(1011,'Jino','Patulli','profession','j.patulli@biggym.com','Jino Patulli  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','patulli.jpg','patulli.png','',''),(1012,'Rodolfo','Evaristo','profession','r.evaristo@gmail.com','Rodolfo Evaristo  is an American fitness professional, the creator of the Sitaras Method and the founder of Sitaras Fitness in New York City. The method developed by him supposes an initial comprehensive evaluation system similar to a general medical examination, in order to design individualized routines according to each student\'s genetic aptitude, level of fitness, health conditions, and personal goals. As the students make progress, the evaluation is resumed periodically to reassess the routines and track their physical changes. He is the personal trainer of several high-profile people from various fields, like business magnate George Soros, economist and former Federal Reserve Chairman Paul Volcker, former General Electric CEO Jack Welch (who recovered from muscular atrophy under Sitaras\' supervision), journalist Charlie Rose, record executive David Geffen or NASCAR champion Jimmie Johnson (who was the first racing driver to become the Associated Press Male Athlete of the Year and who won six championships).','evaristo.jpg','evaristo.png','','');
/*!40000 ALTER TABLE `instructors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `instructorsofthemonth`
--

DROP TABLE IF EXISTS `instructorsofthemonth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instructorsofthemonth` (
  `EmployeeCode` int(11) NOT NULL,
  `Reason` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`EmployeeCode`),
  CONSTRAINT `employee` FOREIGN KEY (`EmployeeCode`) REFERENCES `instructors` (`EmployeeCode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instructorsofthemonth`
--

LOCK TABLES `instructorsofthemonth` WRITE;
/*!40000 ALTER TABLE `instructorsofthemonth` DISABLE KEYS */;
INSERT INTO `instructorsofthemonth` VALUES (1000,'Arnold won the prize in january 2015 thanks to his phisical strength ,he used his muscle to carry almost every piece of equipment from our old gym'),(1004,'Chuck won the prize in march 2015 for foiling a robbery in our factory.He fought against six opponents without help .We owe him a lot for that.'),(1005,'Casio was the instructors of may. He won the price for his formidable kebab eating skill.The members of our gym were really impressed by his prowlesness'),(1007,'Terry won the prize in february 2015 because he saved two members during a lesson of advance swimming.They weren\'t ready to cross the atlantic ocean on their own.'),(1009,'Gianna was the best instructor of april 2015.She received the most number of positive feedback  from our members.She won the contest for her climbing skills');
/*!40000 ALTER TABLE `instructorsofthemonth` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menuitems`
--

DROP TABLE IF EXISTS `menuitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menuitems` (
  `MenuID` int(11) NOT NULL AUTO_INCREMENT,
  `Parent` int(11) DEFAULT NULL,
  `MenuText` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `MenuURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `MenuIcon` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `order` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`MenuID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menuitems`
--

LOCK TABLES `menuitems` WRITE;
/*!40000 ALTER TABLE `menuitems` DISABLE KEYS */;
INSERT INTO `menuitems` VALUES (1,-1,'our gym','index.html',NULL,2),(2,-1,'Courses list','courseCategories.html',NULL,3),(3,-1,'all courses','allCoursesByCategory.html',NULL,4),(4,-1,'instructors','instructors.html',NULL,5),(5,-1,'our rooms','index.html',NULL,6),(6,-1,'location','location.html',NULL,7),(7,-1,'subscription','index.html',NULL,8),(8,-1,'schedule','index.html',NULL,9),(9,3,'by level','allCoursesByLevel.html',NULL,1),(10,3,'by category','allCoursesByCategory.html',NULL,2),(11,3,'alphabetic order','allCoursesAlphabet.html',NULL,3),(12,-1,'Home','index.html',NULL,1);
/*!40000 ALTER TABLE `menuitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roomimages`
--

DROP TABLE IF EXISTS `roomimages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roomimages` (
  `ImageID` int(11) NOT NULL AUTO_INCREMENT,
  `RoomID` int(11) NOT NULL,
  `Width` int(11) DEFAULT NULL,
  `Height` int(11) DEFAULT NULL,
  `URI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`ImageID`,`RoomID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roomimages`
--

LOCK TABLES `roomimages` WRITE;
/*!40000 ALTER TABLE `roomimages` DISABLE KEYS */;
/*!40000 ALTER TABLE `roomimages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rooms`
--

DROP TABLE IF EXISTS `rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rooms` (
  `RoomID` int(11) NOT NULL,
  `Name` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `MapImage` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  `Roomscol` varchar(45) DEFAULT NULL,
  `BackgroundURI` varchar(512) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`RoomID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rooms`
--

LOCK TABLES `rooms` WRITE;
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` VALUES (1,'Yoga Room','yogamap.ong','8','yoga.jpg'),(2,'Swimming Pool','swimmingmap.ong','9','swimming.jpg'),(3,'Pilates Room','pilates.ong','7','pilates.jpg');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teaches`
--

DROP TABLE IF EXISTS `teaches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teaches` (
  `EmployeeCode` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  PRIMARY KEY (`EmployeeCode`,`CourseID`),
  KEY `CourseTeaches_idx` (`CourseID`),
  CONSTRAINT `CourseTeaches` FOREIGN KEY (`CourseID`) REFERENCES `courses` (`CourseID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `InstructorTeaches` FOREIGN KEY (`EmployeeCode`) REFERENCES `instructors` (`EmployeeCode`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teaches`
--

LOCK TABLES `teaches` WRITE;
/*!40000 ALTER TABLE `teaches` DISABLE KEYS */;
INSERT INTO `teaches` VALUES (1000,1),(1000,2),(1007,3),(1000,4),(1007,5),(1004,6),(1004,7),(1004,8),(1007,9);
/*!40000 ALTER TABLE `teaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'biggym'
--

--
-- Dumping routines for database 'biggym'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-14 23:07:01
