function menuToggle() {
    //only toggle content if on phone
    if ($(window).width()<=480) $("body").toggleClass("dialog-open");
    $("#menuBar").animate({
        height: "toggle"
      }, 250, function () {
        // Animation complete.
      });
    
}
function populateMenu(){
	function printSubMenus(elements){
		var subMenuHtml="";
		if ((elements.length)>0){
			subMenuHtml+="<ul class=\"subMenu\">"
		  $.each( elements, function( key, val ) {
			  
			  subMenuHtml+="<a href=" + val.MenuURI + "><li>"+ (val.MenuText.toUpperCase());
			  subMenuHtml+="</li></a>";
			  //subMenuHtml+=printSubMenus(val.SubItems);
			  
		  });
		  subMenuHtml+="</ul>"
		}
		return subMenuHtml;
	}
	$.getJSON( "dataprovider/menuData.php?type=menuItems", function( data ) {
		var menuHtml="<ul>";
	  $.each( data, function( key, val ) {
		  menuHtml+="<a href=" + val.MenuURI + "><li>"+ (val.MenuText.toUpperCase());
		  menuHtml+="</li></a>";
		  menuHtml+=printSubMenus(val.SubItems);
	  });
        menuHtml+="</ul>";
	  $("#menuBar").append(menuHtml);
	});
			
}
$(document).ready(function() {
	populateMenu();
    $("#menuButton").click(menuToggle);
    $("#menuText").click(menuToggle);
    $( "#menuBar" ).hide();
    
});