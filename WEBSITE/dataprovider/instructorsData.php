<?php
	include 'config/dbconfig.inc.php';
	 if ($_REQUEST["type"]=="instructorsDetails"){
		$instructorsDetailsArray= array();
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT *  FROM instructors WHERE EmployeeCode=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$instructor= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$instructor=$row;
			}
		} 
        $sql = "SELECT instructorimages.* FROM  instructors,instructorimages WHERE instructors.EmployeeCode=instructorimages.EmployeeCode AND instructors.EmployeeCode=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$images= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$images[]=$row;
			}
		} 

        $sql = "SELECT courses.* FROM instructors,teaches,courses WHERE instructors.EmployeeCode=teaches.EmployeeCode AND courses.CourseID=teaches.CourseID AND instructors.EmployeeCode=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$course= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$course[]=$row;
			}
		}
        
        $sql = "SELECT instructorsofthemonth.* FROM instructors,instructorsofthemonth WHERE instructors.EmployeeCode=instructorsofthemonth.EmployeeCode  AND        instructors.EmployeeCode=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$month= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$month[]=$row;
			}
		}

        $instructorsDetailsArray["instructor"]=$instructor;
        $instructorsDetailsArray["course"]=$course;
        $instructorsDetailsArray["images"]=$images;
        $instructorsDetailsArray["month"]=$month;
            
		echo json_encode($instructorsDetailsArray);
		$conn->close();
    }
    if ($_REQUEST["type"]=="allInstructors"){
        $conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        } 
        $sql = "SELECT *  FROM instructors ORDER BY instructors.FirstName,instructors.LastName";
        $result = $conn->query($sql);
        $allInstructorsArray= array();
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $allInstructorsArray[]=$row;
            }
        }
        echo json_encode($allInstructorsArray);
        $conn->close();
    }
?>