<?php
	include 'config/dbconfig.inc.php';
	function searchSubMenus($conn,$parent){
		$sql = "SELECT * FROM menuitems WHERE menuitems.Parent=".$parent." ORDER BY menuitems.order";
		$result = $conn->query($sql);
		$subItems=array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$subItems[]=$row;
				//$row["SubItems"]=searchSubMenus($conn,$row["MenuID"]);
			}
			
		} 
		return $subItems;
		

	}
	$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 
	if ($_REQUEST["type"]=="menuItems"){
		$sql = "SELECT * FROM menuitems WHERE menuitems.Parent=-1 ORDER BY menuitems.order";
		$result = $conn->query($sql);
		$menuItems= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$row["SubItems"]=searchSubMenus($conn,$row["MenuID"]);
				$menuItems[]=$row;
			}
			
		} else {
			echo "0 results";
		}
		echo json_encode($menuItems);
		
	}
	
	
	$conn->close();
?>