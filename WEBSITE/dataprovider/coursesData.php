<?php
	include 'config/dbconfig.inc.php';
	if ($_REQUEST["type"]=="categoryList"){
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT * FROM categories ORDER BY orderID asc";
		$result = $conn->query($sql);
		$categories= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$categories[]=$row;
			}
		} else {
			echo "0 results";
		}
		echo json_encode($categories);
		$conn->close();
	}


    if ($_REQUEST["type"]=="coursesList"){
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT * FROM courses WHERE category=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$categories= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$categories[]=$row;
			}
		} else {
			echo "0 results";
		}
		echo json_encode($categories);
		$conn->close();
	}


	if ($_REQUEST["type"]=="categoryDetails"){
		$categoryDetailsArray= array();
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT courseID,Level,Name,courselevels.ImageURI AS ImageURI FROM courses,courselevels WHERE courselevels.LevelID=courses.level AND category=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$courses= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$courses[]=$row;
			}
		} 
		$sql = "SELECT DISTINCT instructors.EmployeeCode, instructors.FirstName, instructors.LastName, instructors.PortraitURI FROM instructors,teaches,courses WHERE courses.category=".$_REQUEST["id"]." AND teaches.CourseID = courses.CourseID AND teaches.EmployeeCode = instructors.EmployeeCode";
		$result = $conn->query($sql);
		$instructors= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$instructors[]=$row;
			}
		} 
		$sql = "SELECT * FROM categories WHERE ID =".$_REQUEST["id"];
		$result = $conn->query($sql);
		$categories= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$categories=$row;
			}
		}
		$sql = "SELECT * FROM categoriesimages WHERE CategoryID =".$_REQUEST["id"];
		$result = $conn->query($sql);
		$categoriesimages= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$categoriesimages[]=$row;
			}
		}
		$sql = "SELECT * FROM courselevels ";
		$result = $conn->query($sql);
		$levels= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$levels[]=$row;
			}
		}
		
		
		$categoryDetailsArray["category"]=$categories;
		$categoryDetailsArray["categoriesimages"]=$categoriesimages;
		$categoryDetailsArray["levels"]=$levels;
		$categoryDetailsArray["instructors"]=$instructors;
		$categoryDetailsArray["courses"]=$courses;
		echo json_encode($categoryDetailsArray);
		$conn->close();
	}



	if ($_REQUEST["type"]=="courseDetails"){
		$courseDetailsArray= array();
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT courses.*,courselevels.LevelName AS LevelName,courselevels.LevelFullName AS LevelFullName,courselevels.ImageURI AS ImageURI FROM courses,courselevels WHERE courseID=".$_REQUEST["id"]." AND courselevels.LevelID=courses.Level";
		$result = $conn->query($sql);
		$course= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$course=$row;
			}
		} 
        $sql = "SELECT * FROM courselevels ";
		$result = $conn->query($sql);
		$levels= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$levels[]=$row;
			}
		}
        $sql = "SELECT instructors.EmployeeCode, instructors.FirstName, instructors.LastName, instructors.PortraitURI FROM instructors,teaches WHERE instructors.EmployeeCode=teaches.EmployeeCode AND teaches.CourseID=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$instructors= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$instructors[]=$row;
			}
		} 
        $sql = "SELECT rooms.* FROM rooms,courses WHERE rooms.RoomID=courses.RoomID AND courses.CourseID=".$_REQUEST["id"];
		$result = $conn->query($sql);
		$rooms= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$rooms=$row;
			}
		} 
		$courseDetailsArray["course"]=$course;
        $courseDetailsArray["levels"]=$levels;
        $courseDetailsArray["instructors"]=$instructors;
        $courseDetailsArray["rooms"]=$rooms;
        //$courseDetailsArray["room"]=$room;
		echo json_encode($courseDetailsArray);
		$conn->close();
	}
    if ($_REQUEST["type"]=="courseByAlphabetical"){
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT courseID,Level,name,courselevels.LevelName AS LevelName,courselevels.LevelFullName AS LevelFullName,courselevels.ImageURI AS ImageURI FROM courses,courselevels WHERE courselevels.LevelID=courses.Level ORDER BY Name";
		$result = $conn->query($sql);
		$courses= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$courses[]=$row;
			}
		} else {
			echo "0 results";
		}
		echo json_encode($courses);
		$conn->close();
	}
    if ($_REQUEST["type"]=="courseByLevel"){
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT courseID,Level,Name,courselevels.LevelName AS LevelName,courselevels.LevelFullName AS LevelFullName,courselevels.ImageURI AS ImageURI FROM courses,courselevels WHERE courselevels.LevelID=courses.Level ORDER BY Level,Name";
		$result = $conn->query($sql);
		$courses= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$courses[]=$row;
			}
		} else {
			echo "0 results";
		}
		echo json_encode($courses);
		$conn->close();
	}
    if ($_REQUEST["type"]=="courseByCategory"){
		$conn = new mysqli($dbservername, $dbusername, $dbpassword, $dbname);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		} 
		$sql = "SELECT courseID,Level,courses.Name as courseN,courselevels.LevelName AS LevelName,courselevels.LevelFullName AS LevelFullName,courselevels.ImageURI AS ImageURI ,categories.Name FROM courses,categories,courselevels WHERE courselevels.LevelID=courses.Level AND categories.ID=courses.Category ORDER BY categories.Name,courses.Name";
		$result = $conn->query($sql);
		$courses= array();
		if ($result->num_rows > 0) {
			// output data of each row
			while($row = $result->fetch_assoc()) {
				$courses[]=$row;
			}
		} else {
			echo "0 results";
		}
		echo json_encode($courses);
		$conn->close();
	}
?>